<?php

namespace Hestec\ElementalExtensions\Elements;

use DNADesign\Elemental\Models\BaseElement;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\NumericField;
use SilverStripe\Forms\OptionsetField;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\TreeDropdownField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\HeaderField;
use SilverStripe\ORM\ArrayList;
use SilverStripe\View\ArrayData;

class ElementContentRss extends BaseElement
{

    private static $table_name = 'HestecElementContentRss';

    private static $singular_name = 'Content & Rss element';

    private static $plural_name = 'Content & Rss elements';

    private static $description = 'Adds a element with content & Rss feed';

    private static $icon = 'font-icon-news';

    private static $db = [
        'Content' => 'HTMLText',
        'RssTitle' => 'Varchar(255)',
        'RssFeed' => 'Varchar(255)',
        'RssNumberToShow' => 'Int()',
        'RssType' => "Enum('1,2','2')",
        'ContentAlign' => "Enum('start,center,end','start')",
        'ContentValign' => "Enum('start,center,end','start')",
        'RssPosition' => "Enum('left,right', 'left')",
        'LinkExtern' => 'Varchar(255)',
        'LinkText' => 'Varchar(255)',
        'LinkTitle'  => 'Varchar(255)',
        'LinkAlign' => "Enum('LEFT,CENTER,RIGHT','LEFT')",
        'LinkFaIcon' => 'Varchar(25)',
        'LinkFaIconPos' => "Enum('LEFT,RIGHT','LEFT')",
        'LinkButton' => 'Boolean',
        'Border' => 'Boolean'
    ];

    private static $defaults = array(
        'Border' => true
    );

    private static $has_one = array(
        'LinkIntern' => SiteTree::class
    );

    public function getCMSFields()
    {

        $fields = parent::getCMSFields();

        $ContentHeaderField = HeaderField::create('ContentHeader', "Content");
        $ContentField = HTMLEditorField::create('Content', "Content");
        $ContentField->setRows(5);
        $ContentAlignField = DropdownField::create('ContentAlign', "ContentAlign", $this->dbObject('ContentAlign')->enumValues());
        $ContentAlignField->setDescription("Horizontal position of content");
        $ContentValignField = DropdownField::create('ContentValign', "ContentValign", $this->dbObject('ContentValign')->enumValues());
        $ContentValignField->setDescription("Vertical position of content");

        $LinkHeaderField = HeaderField::create('LinkHeader', "Link");
        $LinkInternField = TreeDropdownField::create('LinkInternID', "LinkIntern", SiteTree::class);
        $LinkExternField = TextField::create('LinkExtern', "LinkExtern");
        $LinkTextField = TextField::create('LinkText', "LinkText");
        $LinkTitleField = TextField::create('LinkTitle', "LinkTitle");
        $LinkAlignField = DropdownField::create('LinkAlign', "LinkAlign", $this->dbObject('LinkAlign')->enumValues());
        $LinkFaIconField = TextField::create('LinkFaIcon', "LinkFaIcon");
        $LinkFaIconPosField = DropdownField::create('LinkFaIconPos', "LinkFaIconPos", $this->dbObject('LinkFaIconPos')->enumValues());
        $LinkButtonField = CheckboxField::create('LinkButton', "LinkButton");
        $BorderField = CheckboxField::create('Border', "Border");

        $RssHeaderField = HeaderField::create('RssHeader', "Rss feed");
        $RssTitleField = TextField::create('RssTitle', "RssTitle");
        $RssFeedField = TextField::create('RssFeed', "RssFeed");
        $RssNumberToShowField = NumericField::create('RssNumberToShow', "Number of messages");
        $RssNumberToShowField->setDescription("Default: 5");
        $RssTypeField = DropdownField::create('RssType', "RssType", $this->dbObject('RssType')->enumValues());
        $RssTypeField->setDescription("If the Rss is not working well, try another type");
        $RssPositionField = OptionsetField::create('RssPosition', "RssPosition", $this->dbObject('RssPosition')->enumValues());
        $RssPositionField->setDescription("Card with Rss left or right from the text.");
        $RssPositionField->addExtraClass("layout-horizontal");

        $fields->addFieldToTab('Root.Main', $ContentHeaderField);
        $fields->addFieldToTab('Root.Main', $ContentField);
        $fields->addFieldToTab('Root.Main', $ContentAlignField);
        $fields->addFieldToTab('Root.Main', $ContentValignField);
        $fields->addFieldToTab('Root.Main', $RssHeaderField);
        $fields->addFieldToTab('Root.Main', $RssTitleField);
        $fields->addFieldToTab('Root.Main', $RssFeedField);
        $fields->addFieldToTab('Root.Main', $RssTypeField);
        $fields->addFieldToTab('Root.Main', $RssNumberToShowField);
        $fields->addFieldToTab('Root.Main', $LinkHeaderField);
        $fields->addFieldToTab('Root.Main', $LinkInternField);
        $fields->addFieldToTab('Root.Main', $LinkExternField);
        $fields->addFieldToTab('Root.Main', $LinkTextField);
        $fields->addFieldToTab('Root.Main', $LinkTitleField);
        $fields->addFieldToTab('Root.Main', $LinkAlignField);
        $fields->addFieldToTab('Root.Main', $LinkFaIconField);
        $fields->addFieldToTab('Root.Main', $LinkFaIconPosField);
        $fields->addFieldToTab('Root.Main', $LinkButtonField);
        $fields->addFieldToTab('Root.Main', $RssPositionField);
        $fields->addFieldToTab('Root.Main', $BorderField);

        return $fields;

    }

    public function getType()
    {
        return 'Content & Rss';
    }

    public function getRssItems(){

        if (filter_var($this->RssFeed, FILTER_VALIDATE_URL)) {

            $client = new \GuzzleHttp\Client();
            $request = $client->request('GET', $this->RssFeed);

            //$conn = $fetch->request();

            $msgs = simplexml_load_string($request->getBody());

            //generateanobjectourtemplatescanread
            $output = new ArrayList();

            if ($msgs) {

                if ($this->RssType == 1) {

                    foreach ($msgs->channel->item as $msg) {
                        $output->push(new ArrayData(array(
                            'Title' => $msg->title->__toString(),
                            'Link' => $msg->link->__toString()
                        )));
                    }

                }else{

                    foreach ($msgs->entry as $msg) {
                        $output->push(new ArrayData(array(
                            'Title' => strip_tags($msg->title->__toString()),
                            'Link' => $msg->link['href']->__toString()
                        )));
                    }

                }

            }

            return $output;

        }

        return false;

    }

    public function NumberOfMessages(){

        if ($this->RssNumberToShow > 0){
            return $this->RssNumberToShow;
        }
        return 5;

    }

}