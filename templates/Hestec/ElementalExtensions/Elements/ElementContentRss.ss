<div class="container">
    <div class="row">
        <div class="col-12 col-lg-6 d-flex flex-column justify-content-$ContentValign<% if $ContentAlign = center %> text-center<% end_if %><% if $ContentAlign = end %> text-right<% end_if %><% if $RssPosition != right %> order-lg-2<% end_if %>">
            <% if $ShowTitle %><h2<% if $HSize && $HSize != normal %> class="$HSize"<% end_if %>>$Title.RAW</h2><% end_if %>
            $Content
            <% if $LinkExtern || $LinkIntern %><a href="<% if $LinkIntern %>$LinkIntern.AbsoluteLink<% else %>$LinkExtern<% end_if %>" title="$LinkTitle"<% if $LinkExtern %> target="_blank"<% end_if %>><span class="icon $LinkFaIcon"></span> $LinkText</a><% end_if %>
        </div>
        <div class="col-12 col-lg-6<% if $RssPosition != right %> order-lg-1<% end_if %>">
            <div class="card card-main">
                <div class="card-header">
                <% if $RssTitle %><h3>$RssTitle</h3><% end_if %>
                </div>
                <div class="card-body">
                <% if $getRssItems %>
                    <ul class="list-hand">
                <% loop $getRssItems.Limit($NumberOfMessages) %>
                    <li><a href="$Link" title="$Title" target="_blank">$Title</a></li>
                <% end_loop %>
                    </ul>
                <% else %>
                    <p>Invalid feed url...</p>
                <% end_if %>
                </div>
            </div>
        </div>
    </div>
</div>

